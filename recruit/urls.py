from django.urls import path
from .views import (
    home, siths, recruits, questions, sith_detail,
    recruit_answers, recruit_accept)


urlpatterns = [
    path('', home, name='home'),
    path('siths/', siths, name='siths'),
    path('siths/<int:sith_id>/', sith_detail, name='sith_detail'),
    path('siths/<int:sith_id>/recruit/<int:recruit_id>/', recruit_answers,
         name='recruit_answers'),
    path('siths/<int:sith_id>/recruit/<int:recruit_id>/accept/', recruit_accept,
         name='recruit_accept'),
    path('recruits/', recruits, name='recruits'),
    path('recruits/<int:recruit_id>/questions/', questions,
         name='questions'),
]
