from django.contrib import admin
from .models import Recruit, Sith, Planet, Questions


admin.site.register(Recruit)
admin.site.register(Sith)
admin.site.register(Planet)
admin.site.register(Questions)
