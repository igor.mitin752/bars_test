from django.db import models


class Planet(models.Model):
    name = models.CharField(max_length=35, verbose_name='Название планеты')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Планета'
        verbose_name_plural = 'Планеты'


class Sith(models.Model):
    name = models.CharField(max_length=20, verbose_name='Имя ситха')
    planet = models.ForeignKey(Planet, verbose_name='Планета обитания ситха',
                               on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Ситх'
        verbose_name_plural = 'Ситхи'


class Recruit(models.Model):
    sith = models.ForeignKey(Sith, blank=False, null=True,
                             on_delete=models.CASCADE)
    name = models.CharField(max_length=35, verbose_name='Имя рекрута')
    planet = models.ForeignKey(Planet, verbose_name='Планета обитания рекрута',
                               on_delete=models.CASCADE)
    age = models.IntegerField(verbose_name='Возраст')
    email = models.EmailField(verbose_name='Почта рекрута')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Рекрут'
        verbose_name_plural = 'Рекруты'


class Questions(models.Model):
    question = models.CharField(max_length=35, verbose_name='Вопрос')

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = 'Вопрос для получения руки Тени'
        verbose_name_plural = 'Вопросы для получения руки Тени'


class RecruitAnswers(models.Model):
    recruit = models.ForeignKey(Recruit, verbose_name='Рекрут',
                                on_delete=models.CASCADE)
    question = models.ForeignKey(Questions, verbose_name='Вопрос',
                                 on_delete=models.CASCADE)
    answer = models.BooleanField(verbose_name='Ответ рекурта')

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = 'Ответ рекрута'
        verbose_name_plural = 'Ответы рекрута'
