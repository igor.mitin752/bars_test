from django.core.mail import EmailMultiAlternatives
from django.db.models import Count, OuterRef, Exists
from django.shortcuts import (
    render, redirect, get_object_or_404)
from django.template.loader import get_template

from .forms import RecruitRegisterForm
from .models import Sith, Questions, Recruit, RecruitAnswers


def home(request):
    return render(request, 'recruit/homepage.html')


def siths(request):
    more = request.GET.get('more', 0)

    siths = Sith.objects.annotate(
        rec_count=Count('recruit')).order_by('name')
    if more:
        siths = siths.filter(
            rec_count__gt=1,
        )

    return render(request, 'recruit/siths.html', {
        'siths': siths
    })


def sith_detail(request, sith_id):
    sith = Sith.objects.get(pk=sith_id)
    answers = RecruitAnswers.objects.filter(recruit=OuterRef('pk'))
    recruits = Recruit.objects.filter(
        Exists(answers),
        sith__isnull=True
    ).order_by('name')

    data = {
        'sith': sith,
        'recruits': recruits
    }
    return render(request, 'recruit/sith_detail.html', data)


def recruit_answers(request, sith_id, recruit_id):
    sith = Sith.objects.annotate(
        rec_count=Count('recruit')
    ).get(pk=sith_id)
    recruit = Recruit.objects.get(pk=recruit_id)
    answers = RecruitAnswers.objects.filter(recruit_id=recruit_id)
    data = {
        'sith': sith,
        'recruit': recruit,
        'answers': answers
    }
    return render(request, 'recruit/recruit_answers.html', data)


def recruit_accept(request, sith_id, recruit_id):
    sith = Sith.objects.get(pk=sith_id)
    recruit = Recruit.objects.get(pk=recruit_id)

    recruit.sith = sith
    recruit.save()

    # Отправка уведомления
    msg_plaintext = get_template('messages/template.txt')
    msg_html = get_template('messages/template.html')
    context = {
        'sith': sith,
        'recruit': recruit
    }

    subject = 'Вас назначили Рукой Тени'
    from_email = 'robot@sw.glx'
    to = recruit.email
    text_content = msg_plaintext.render(context)
    html_content = msg_html.render(context)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

    return redirect('siths')


def recruits(request):
    if request.method == 'POST':
        form = RecruitRegisterForm(request.POST)
        if form.is_valid():
            recruit = form.save()
            return redirect('questions', recruit_id=recruit.id)
    else:
        form = RecruitRegisterForm()
    return render(request, 'recruit/recruit_register.html', {'form': form})


def questions(request, recruit_id):
    recruit = get_object_or_404(Recruit, pk=recruit_id)
    question_id = 0

    if request.method == 'POST':
        question_id = int(request.POST.get('question_id'))
        rq = get_object_or_404(Questions, pk=question_id)
        answer = bool(request.POST.get('answer'))
        # Сохраняем
        ra = RecruitAnswers(recruit=recruit, question=rq, answer=answer)
        ra.save()

    # Первые 2 вопроса с id > текущего (или 0)
    question = Questions.objects.filter(
        pk__gt=question_id
    ).order_by('pk')[:2]
    if question:
        data = {
            'question': question[0],
            'is_last': len(question) == 1,  # Вопрос один == последний
            'answer': False  # Ответ рекрута. Тут всегда False по дефолту
        }

        response = render(request, 'recruit/questions.html', data)
    else:
        response = redirect('home')

    return response
