from django import forms

from .models import Recruit


class RecruitRegisterForm(forms.ModelForm):

    class Meta:
        model = Recruit
        fields = ['name', 'planet', 'age', 'email']
